# Hiatus

# DOCKER 
docker-compose -f docker-compose-build.yml build --no-cache
docker-compose -f docker-compose-build.yml up --force-recreate
docker-compose -f docker-compose-build.yml up --build

docker exec -it -u 0 hiatus_django python3 manage.py collectstatic --no-input


docker-compose -f docker-compose-geoserver.yml up 

## Geoserver setup for WMTS serving
docker pull kartoza/geoserver:2.21.0
curl --noproxy "*" -v -u admin:myawesomegeoserver -XPUT -H "Content-type: image/tiff" --data-binary @wsiearth2.tif http://localhost:8080/geoserver/rest/workspaces/mort/coveragestores/wsiearth2/file.geotiff


### Without POSTGIS DB link
docker run -d -p 8600:8080 --name geoserver -e GEOSERVER_ADMIN_USER=mzellou  -e GEOSERVER_ADMIN_PASSWORD=test -e STABLE_EXTENSIONS=charts-plugin,db2-plugin -t kartoza/geoserver:2.21.0
### With POSTGIS DB link
docker run -d -p 5432:5432 --name db kartoza/postgis:13.0
docker run -d -p 8600:8080 --name geoserver --link db:db -e DB_BACKEND=POSTGRES -e HOST=db -e POSTGRES_PORT=5432 -e POSTGRES_DB=gis -e POSTGRES_USER=docker -e POSTGRES_PASS=docker kartoza/geoserver:2.21.0

Once the server is up, set up a SSH tunnel and go to http://localhost:8600/geoserver/web/ on your local machine
For ssh tunnel : ssh -L 8600:localhost:8600 dl@HP1710W028.ign.fr

TODO docker compose with postgis+geoserver

### Add raster files to postgis

### Create WMTS from POSTGIS DB in Geoserver

## Django for serving both WMTS at the same time
Use sentinel or OSM or Google Earth WMTS ?
https://www.earthdata.nasa.gov/eosdis/science-system-description/eosdis-components/gibs
https://www.sentinel-hub.com/develop/api/ogc/standard-parameters/wmts/
https://scihub.copernicus.eu/dhus/#/home
https://towardsdatascience.com/how-to-download-high-resolution-satellite-data-for-anywhere-on-earth-5e6dddee2803

## QGIS server
docker run --name "qgis-server" -v /media/hd/hiatus/data/project/:/project -p 8080:80 -d -t kartoza/qgis-server:LTR
docker run --name "qgis-server" -v /mnt/remote/hiatus/data/project/:/project -p 8080:80 -d -t kartoza/qgis-server:LTR

docker run --name "qgis-server" -e QGIS_PROJECT_FILE='' -v /mnt/remote/hiatus/data/project/:/gis -p 8080:80 -d -t kartoza/qgis-server:LTR
docker run --name "qgis-server" -e QGIS_PROJECT_FILE='' -v /home/MZellou/py/hiatus/data/project/:/gis -p 8080:80 -d -t kartoza/qgis-server:LTR
docker run --name "qgis-server" -e QGIS_PROJECT_FILE='old_mosaic.qgs' -v /home/MZellou/py/hiatus/data/project/:/project -p 8080:80 -d -t kartoza/qgis-server:LTR

## Geoserver
docker run -d -p 8600:8080 --name geoserver -e GEOSERVER_ADMIN_USER=mzellou  -e GEOSERVER_ADMIN_PASSWORD=test -e STABLE_EXTENSIONS=charts-plugin,db2-plugin -t kartoza/geoserver:2.21.0



# CR 21/10/2022

Orientation interne : mettre toutes les mêmes images dans un meme réf amera. Lors de la prise de vue, les films peuvent être un peu bougés et lors du scan aussi.
Pour le faire on utilise les repères de fond de chambre 

Relative : orientation des images par rapport à une première 

Mise en place absolue : on utilise les métadonnées de position des images (approx sommet de prise de vue)

==> Equation à partir de ces élements
géoreferencement grossier dans le repère terrain
=> ortho+ mns 

AJout Arnaud : 
Détection auto point d'appui
détection auto de pt homologues entre ancienne qui vient d'être produite et bdortho nouvelle. D'abord dallage puis détection sur la dalle

REcalcul avec les nouvelles coord

D2tection pts homologues : 
Cf ppt

D2tection automatique des pts homologues : superglue à tester

Pts blocants : 
orientation interne compliquée : les repères peuvent être similaires à des repères de fdc
le calcul des résidus peut aider à trouver ces repères problématiques (résidus forts peuvent nécessiter correction manuelle)

Mise en place en l'air : si pas assez ds pts le modèle part en vrille. Pb on s'en rend compte pendant ou à la fin du traitement. L'erreur n'est pas forcément comréhensible et ne rend pas forcément compte du cliché problématique. 
PM a essayer d'apparier par bandes avant de faire de l'interbandes

PB : il est possible d'avoir deux caméras différentes pour la même prise de vue. Dans les métadonnées ce n'est pas forcément marqué
===================================================================================================================

## Chantier//

Il existait un outil qui lancait le DL depuis remonter le temps pour DL les métadonnées des PDV avec une bbox. Allégoria (à voir)

TEster si possible d'utiliser TA.exe sur linux
