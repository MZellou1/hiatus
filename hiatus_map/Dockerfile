# From https://github.com/thinkWhere/GDAL-Docker/tree/develop/3.8-ubuntu
#### Use latest Ubuntu
FROM ubuntu:focal-20220426

ARG apt_mirror_name="ign"
ENV APT_MIRROR_NAME=${apt_mirror_name}

# WORKDIR /home/dlsupport/Documents/marouane/hiatus
# RUN ls 

#RUN sed -i 's/security.ubuntu.com/miroir.ign.fr/g' /etc/apt/sources.list
#RUN rm /etc/apt/sources.list
RUN echo "deb http://miroir.ign.fr/ubuntu/ focal main restricted universe multiverse \n deb http://miroir.ign.fr/ubuntu/ focal-updates main restricted universe multiverse \n deb http://miroir.ign.fr/ubuntu-security/ focal-security main restricted universe multiverse" > /etc/apt/sources.list

RUN cat /etc/apt/sources.list

#--------------------------------------------------------------------------------
# Configuration du proxy valable pour toutes les images dérivées
#--------------------------------------------------------------------------------
ARG http_proxy=""
ENV http_proxy=${http_proxy}
ENV HTTP_PROXY=${http_proxy}

ARG https_proxy=""
ENV https_proxy=${https_proxy}
ENV HTTPS_PROXY=${https_proxy}

ARG no_proxy=""
ENV no_proxy=${no_proxy}
ENV NO_PROXY=${no_proxy}

ENV TZ 'FR'

# fix "The method driver /usr/lib/apt/methods/https could not be found."

# Update base container install
RUN apt-get update && echo $TZ > /etc/timezone && \
    apt-get install -y tzdata && \
    rm /etc/localtime && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
    apt-get clean

# Install GDAL dependencies
RUN apt-get install -y python3-pip libgdal-dev locales nodejs npm
RUN pip install --proxy=${https_proxy} --upgrade pip

# Ensure locales configured correctly
RUN locale-gen fr_FR.UTF-8
ENV LC_ALL='fr_FR.UTF-8'

# Set python aliases for python3
RUN echo 'alias python=python3' >> ~/.bashrc
RUN echo 'alias pip=pip3' >> ~/.bashrc

# Update C env vars so compiler can find gdal
ENV CPLUS_INCLUDE_PATH=/usr/include/gdal
ENV C_INCLUDE_PATH=/usr/include/gdal

# This will install 3.0.4 version of GDAL
RUN pip3 install GDAL==3.0.4

RUN apt-get update && apt-get install -y libspatialindex-dev
RUN apt-get update


########################################################################################################
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN mkdir -p /home/hiatus

RUN addgroup --system app && adduser app --ingroup app  --system --no-create-home --disabled-login --disabled-password 

ENV HOME=/home/hiatus
ENV APP_HOME=/home/hiatus/web
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

# Force service restart to yes
RUN echo '* libraries/restart-without-asking boolean true' | debconf-set-selections
# RUN apt-get -o Dpkg::Options::='--force-confold' --force-yes -fuy dist-upgrade"
RUN DEBIAN_FRONTEND=noninteractive && apt-get update && apt-get -y upgrade

COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY ./entrypoint-django.sh .
RUN sed -i 's/\r$//g'  $APP_HOME/entrypoint-django.sh

COPY . $APP_HOME

RUN npm init -y
RUN npm install browserify
RUN npm install leaflet-side-by-side
RUN python3 manage.py collectstatic --noinput 
RUN python3 manage.py npminstall

RUN chown -R app:app $APP_HOME
RUN chown -R app:app /home/hiatus/web/map/staticfiles/

USER app
RUN chmod +x  $APP_HOME/entrypoint-django.sh
RUN chmod 755 $APP_HOME/entrypoint-django.sh

RUN ls
RUN pwd

ENTRYPOINT [ "bash", "-c", "./entrypoint-django.sh" ]