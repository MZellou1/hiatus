#!/bin/sh

set -e

#python3 manage.py flush --no-input
#python3 manage.py npminstall
#python3 manage.py collectstatic --noinput #--clear
python3 manage.py makemigrations
python3 manage.py migrate --noinput

gunicorn hiatus_map.wsgi:application --bind 0.0.0.0:8000  --workers 8 --threads 2

#python3 manage.py runserver 0.0.0.0:8000
#uwsgi --socket :9000 --workers 4 --master --enable-threads --module hiatus_map.wsgi.application
