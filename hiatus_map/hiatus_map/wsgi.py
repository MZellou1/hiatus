"""
WSGI config for hiatus_map project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hiatus_map.settings')

application = get_wsgi_application()

os.environ['http_proxy'] = 'http://proxy.ign.fr:3128' #str(os.system('$HTTP_PROXY'))
os.environ['https_proxy'] = 'http://proxy.ign.fr:3128' #str(os.system('$HTTPS_PROXY'))
os.environ['no_proxy'] = 'localhost,.ign.fr,127.0.0.1,0.0.0.0,[::1]' #str(os.system('$NO_PROXY'))