from django.urls import path, include
from . import views

app_name = "map"


urlpatterns = [ 
    path('send_ajax_bbox/', views.send_ajax_bbox, name='send_ajax_bbox'), 
]