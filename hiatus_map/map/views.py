from django.shortcuts import render
from hiatus_map.settings import BASE_DIR
import json
from django.http.response import HttpResponse, HttpResponseRedirect
from django.http import HttpResponse, Http404, JsonResponse


# Create your views here.
def home(request):
    """_summary_
    Render the leaflet map with preloaded labels of the geographical objects for autocompletion in the search bar
    """
    # if os.path.exists('map/static/json/mapdata_json.geojson'):
    if 'first_log' not in request.session.keys():
        request.session.flush()
        request.session['first_log'] = 1

    return render(request, 'map.html')


def send_ajax_bbox(request):
   # Get BBOX from AJAX request
    # TODO pour la prod
    # if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
    #     request.session['cur_bbox'] = json.loads(request.body)

    request.session['cur_bbox'] = json.loads(request.body)

    data = {
        'bbox_e': json.loads(request.body)['bbox_e'],
        'bbox_w': json.loads(request.body)['bbox_w'],
        'bbox_s': json.loads(request.body)['bbox_s'],
        'bbox_n': json.loads(request.body)['bbox_n']
    }

    return JsonResponse(data)